package com.docotronics.toha_apps

import android.app.Application
import android.content.Context
import android.util.Log
import com.docotronics.toha_apps.network.ConnectionType
import com.docotronics.toha_apps.network.NetworkConnection.networkMonitor
import com.docotronics.toha_apps.network.NetworkMonitorUtil

public class MVVMApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        networkMonitor = NetworkMonitorUtil(this)
        networkMonitor.register()
    }

}