package com.docotronics.toha_apps.model.binding_model


data class LoginBindingModel(
        var userid : String? = "",
        var password : String? = ""
)
