package com.docotronics.toha_apps.model.request_model

data class LoginRequest(
        var username: String? = "",
        var password: String? = ""
){

}
