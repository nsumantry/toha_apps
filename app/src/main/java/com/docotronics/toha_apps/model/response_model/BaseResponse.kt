package com.docotronics.toha_apps.model.response_model


import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("rc")
    val rc: String,

    @SerializedName("rm")
    val rm: String,

    @SerializedName("data")
    val data : BaseDataResponse<T>
)