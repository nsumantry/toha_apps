package com.docotronics.toha_apps.model.response_model


import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("name")
    val name: String,
    @SerializedName("role")
    val role: String
)