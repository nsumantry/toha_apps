package com.docotronics.toha_apps.network

object ApiUtil {
        val BaseURL = "http://60.253.115.138:9994/toha/"
        //val BaseURL = "http://api.ethica.id/public/"


        //RESPONSE CODE
        const val RC_00 = "00"
        const val RC_10 = "10"
        const val RC_11 = "11"
        const val RC_12 = "12"
        const val RC_13 = "13"
        const val RC_14 = "14"
        const val RC_15 = "15"
        const val RC_16 = "16"
        const val RC_17 = "17"
        const val RC_18 = "18"
        const val RC_19 = "19"
        const val RC_50 = "50"
        const val RC_98 = "98"
        const val RC_99 = "99"
        const val RC_IE = "IE"

        //RESPONSE MESSAGE
        const val RM_00 = "Success Request berhasil di ekseskusi"
        const val RM_10 = "User authentication failed Kesalahan pada username/email"
        const val RM_11 = "User not found User tidak terdaftar"
        const val RM_12 = "Pin authentication failed Kesalahan pada pin/password"
        const val RM_13 = "Data already exist Data double"
        const val RM_14 = "User inactive User tidak aktif"
        const val RM_15 = "User blocked User di blokir"
        const val RM_16 = "Data not found Data tidak ditemukan"
        const val RM_17 = "Data invalid Data tidak valid "
        const val RM_18 = "Failed connect to server Service gagal koneksi ke server db"
        const val RM_19 = "Failed save data Gagal menyimpan data"
        const val RM_50 = "Timeout Timeout koneksi ke db"
        const val RM_98 = "Authentication service failed Gagal autentikasi ke service"
        const val RM_99 = "Service path not found Service path tidak ditemukan"
        const val RM_IE = "Internal err"


        val ResponseCode : Map<String, String> = mapOf(
                RC_00 to RM_00,
                RC_10 to RM_10,
                RC_11 to RM_11,
                RC_12 to RM_12,
                RC_13 to RM_13,
                RC_14 to RM_14,
                RC_15 to RM_15,
                RC_16 to RM_16,
                RC_17 to RM_17,
                RC_18 to RM_18,
                RC_19 to RM_19,
                RC_50 to RM_50,
                RC_98 to RM_98,
                RC_99 to RM_99,
                RC_IE to RM_IE
        )

}



