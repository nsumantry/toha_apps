package com.docotronics.toha_apps.network

import com.docotronics.toha_apps.utils.NoInternetException

object NetworkConnection {

    public lateinit var networkMonitor : NetworkMonitorUtil

    fun getInternetConnection() : Boolean{
        return networkMonitor.result
    }

}