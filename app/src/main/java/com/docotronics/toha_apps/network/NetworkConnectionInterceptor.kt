package com.docotronics.toha_apps.network

import com.docotronics.toha_apps.network.NetworkConnection.getInternetConnection
import com.docotronics.toha_apps.utils.NoInternetException
import okhttp3.Interceptor
import okhttp3.Response

class NetworkConnectionInterceptor() : Interceptor{

    override fun intercept(chain: Interceptor.Chain) : Response {
        if (!getInternetConnection()){
            throw NoInternetException("Pastiken terhubung ke internet")
        }

        try {
            return chain.proceed(chain.request())
        }catch (E : Exception){
            throw E
        }

    }
}