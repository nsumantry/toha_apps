package com.docotronics.toha_apps.network

import com.docotronics.toha_apps.network.service.LoginService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitApp {

    val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(NetworkConnectionInterceptor())
            .build()

    val retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(ApiUtil.BaseURL)
            .build()

    fun getLoginService() : LoginService {
        return retrofit.create(LoginService::class.java)
    }
}