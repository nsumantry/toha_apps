package com.docotronics.toha_apps.network

import com.docotronics.toha_apps.model.request_model.LoginRequest
import com.docotronics.toha_apps.model.response_model.BaseResponse
import com.docotronics.toha_apps.model.response_model.LoginResponse
import com.docotronics.toha_apps.network.ApiUtil.BaseURL
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface api {

    @Headers(
            "Content-Type: application/json",
            "secretKey: D0C0TR0N1C5XT0H4"
    )
    @POST("login")
    suspend fun login(
            @Body loginRequest : LoginRequest
    ) : Response<BaseResponse<LoginResponse>>

    companion object{
        operator fun invoke() : api {
            return Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BaseURL)
                    .build()
                    .create(api::class.java)
        }
    }
}