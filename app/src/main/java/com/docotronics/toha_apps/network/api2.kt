package com.docotronics.toha_apps.network

import com.docotronics.toha_apps.model.request_model.LoginRequest
import com.docotronics.toha_apps.model.response_model.BaseResponse
import com.docotronics.toha_apps.model.response_model.LoginResponse
import com.docotronics.toha_apps.network.ApiUtil.BaseURL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


object api2 {

    fun getInterceptor(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        var okhttp = OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .build()

        return okhttp
    }

    fun getNetwork(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BaseURL)
            .client(getInterceptor())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getInterface() : testService{
        return getNetwork().create(testService::class.java)
    }

}

interface testService {
    @GET("master_barang/loaddata?offset=0")
    fun getProduct() : Call<String>

    @Headers(
            "Content-Type:application/json","secretKey:D0C0TR0N1C5XT0H4"
    )
    @POST("login")
    fun login(
            @Body loginRequest: LoginRequest,
    ) : Call<BaseResponse<LoginResponse>>
}