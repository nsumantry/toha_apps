package com.docotronics.toha_apps.network.service

import com.docotronics.toha_apps.model.request_model.LoginRequest
import com.docotronics.toha_apps.model.response_model.BaseResponse
import com.docotronics.toha_apps.model.response_model.LoginResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface LoginService {

    @Headers(
            "Content-Type: application/json",
            "secretKey: D0C0TR0N1C5XT0H4"
    )
    @POST("login")
    suspend fun login(
            @Body loginRequest : LoginRequest
    ) : Response<BaseResponse<LoginResponse>>

}