package com.docotronics.toha_apps.repositroy

import com.docotronics.toha_apps.network.service.LoginService
import com.docotronics.toha_apps.model.request_model.LoginRequest
import com.docotronics.toha_apps.model.response_model.BaseResponse
import com.docotronics.toha_apps.model.response_model.LoginResponse
import com.docotronics.toha_apps.service.SafeResponseApiRequest
import java.lang.Exception

class LoginRepository (
    private val loginService: LoginService
): SafeResponseApiRequest() {

    suspend fun login(loginRequest: LoginRequest) : BaseResponse<LoginResponse> {
        try {
            return apiRequest {
                loginService.login(loginRequest)
            }
        }catch (e : Exception){
            throw e
        }
    }
}