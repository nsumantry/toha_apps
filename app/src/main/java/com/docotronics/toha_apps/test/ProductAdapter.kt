package com.docotronics.toha_apps.test

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.docotronics.toha_apps.R
import com.docotronics.toha_apps.databinding.ItemProductBinding

class ProductAdapter : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private var _products : ProductModel = ProductModel()

    override fun getItemCount() = _products.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ProductViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.item_product,
                        parent,
                        false
                )
        )


    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.itemProductBinding.viewModel = _products[position]
    }

    fun add(productModel : ProductModel){
        val StartPosition : Int = _products.size-1
        _products.addAll(StartPosition, productModel)
        notifyItemRangeChanged(StartPosition, _products.size-1)
    }

    inner class ProductViewHolder(
            val itemProductBinding: ItemProductBinding
    ) : RecyclerView.ViewHolder(itemProductBinding.root)

}