package com.docotronics.toha_apps.test


import com.google.gson.annotations.SerializedName

data class ProductModelItem(
    @SerializedName("brand")
    var brand: String,
    @SerializedName("diskon_pct")
    var diskonPct: String,
    @SerializedName("gambar")
    var gambar: String,
    @SerializedName("gambar_besar")
    var gambarBesar: String,
    @SerializedName("harga")
    var harga: String,
    @SerializedName("keterangan")
    var keterangan: String,
    @SerializedName("klasifikasi")
    var klasifikasi: String,
    @SerializedName("nama")
    var nama: String,
    @SerializedName("seq")
    var seq: String,
    @SerializedName("stok")
    var stok: String,
    @SerializedName("sub_brand")
    var subBrand: String,
    @SerializedName("ukuran")
    var ukuran: String,
    @SerializedName("warna")
    var warna: String
)