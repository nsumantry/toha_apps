package com.docotronics.toha_apps.test

import retrofit2.Response
import java.lang.Exception

class ProductRepository(
    private val productService : ProductService
) : SafeResponseApiRequestTest(){

    suspend fun GetProduct(offset : Int, limit : Int) : ProductModel {
        try{
            return apiRequest {
                productService.GetProduct(offset, limit)
            }
        }catch (e : Exception){
            throw e
        }
    }

}