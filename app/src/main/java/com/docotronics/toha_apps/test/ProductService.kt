package com.docotronics.toha_apps.test

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ProductService {

    @GET("master_barang/loaddata")
    suspend fun GetProduct(
        @Query("offset") offset : Int,
        @Query("limit") limit : Int,
    ) : Response<ProductModel>

}