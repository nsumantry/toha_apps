package com.docotronics.toha_apps.test

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.docotronics.toha_apps.model.binding_model.LoginBindingModel
import com.docotronics.toha_apps.network.ApiUtil
import com.docotronics.toha_apps.repositroy.LoginRepository
import com.docotronics.toha_apps.utils.ApiException
import com.docotronics.toha_apps.utils.Courotines
import com.docotronics.toha_apps.utils.NoInternetException
import com.docotronics.toha_apps.view.login.LoginListener
import kotlinx.coroutines.Job
import java.lang.Exception

class ProductViewModel(
    private val productRepository : ProductRepository
) : ViewModel() {

    private lateinit var job : Job
    var listener : ListenerTest? = null

    private var _ListProduct : MutableLiveData<ProductModel> = MutableLiveData()
    val ListProduct : LiveData<ProductModel>
        get() = _ListProduct


    init {
        _ListProduct = MutableLiveData()
        _ListProduct.value = ProductModel()
    }

    fun LoadBarang(){

        Courotines.main {
            try {
                val productModel : ProductModel = productRepository.GetProduct(_ListProduct.value!!.size, 10)
                _ListProduct.value!!.addAll(_ListProduct.value!!.size, productModel)
                listener?.OnLoad()

            }catch (E : ApiException){
                Log.e("ERROR 1", E.message!!)
            }catch (E : NoInternetException){
                Log.e("ERROR 2", E.message!!)
            }catch (E : Exception){
                Log.e("ERROR 3", E.message!!)
            }
        }

//        job = Courotines.ioTenMain(
//                {productRepository.GetProduct(_ListProduct.value!!.size, 10)}
//                {
//                    _ListProduct.value!!.addAll(_ListProduct.value!!.size, productModel)
////                listener?.OnLoad()
//
//                }
//        )

    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}