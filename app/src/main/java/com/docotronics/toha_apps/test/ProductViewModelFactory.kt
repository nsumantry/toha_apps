package com.docotronics.toha_apps.test

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.docotronics.toha_apps.repositroy.LoginRepository
import com.docotronics.toha_apps.view_model.LoginViewModel

class ProductViewModelFactory(
    private val repository : ProductRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ProductViewModel(repository) as T
    }
}