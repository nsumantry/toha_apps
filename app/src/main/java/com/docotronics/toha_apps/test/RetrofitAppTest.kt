package com.docotronics.toha_apps.test

import com.docotronics.toha_apps.network.NetworkConnectionInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitAppTest {

    val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(NetworkConnectionInterceptor())
            .build()

    val retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://api.ethica.id/public/")
            .build()

    fun getProductService() : ProductService {
        return retrofit.create(ProductService::class.java)
    }
}