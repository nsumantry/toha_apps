package com.docotronics.toha_apps.test

import com.docotronics.toha_apps.utils.ApiException
import retrofit2.Response
import java.lang.Exception

abstract class SafeResponseApiRequestTest {

    suspend fun <T : Any> apiRequest(call : suspend() -> Response<T>) : T {
        try {
            val response = call.invoke()
            if (response.isSuccessful){
                return response.body()!!
            }else{
                throw ApiException(response.code().toString())
            }
        } catch ( e : Exception ) {
            throw e
        }
    }
}