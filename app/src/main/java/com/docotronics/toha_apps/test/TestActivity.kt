package com.docotronics.toha_apps.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.docotronics.toha_apps.R
import com.docotronics.toha_apps.databinding.ActivityTestBinding
import com.docotronics.toha_apps.utils.Toast

class TestActivity : AppCompatActivity(), ListenerTest {

    private lateinit var viewModel: ProductViewModel
    private lateinit var binding: ActivityTestBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        InitClass()
    }

    private fun InitClass() {
        val productService = RetrofitAppTest.getProductService()
        val repository = ProductRepository(productService)
        val factory = ProductViewModelFactory(repository)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_test)
        viewModel = ViewModelProvider(this, factory).get(ProductViewModel::class.java)
        viewModel.listener = this
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

    }

    override fun OnLoad() {
        viewModel.ListProduct.observe(this, Observer {
            Toast(it.size.toString())

//            viewModel.ListProduct.observe(this, Observer {
//                listProduct -> binding.rcvData.also {
//                it.layoutManager = LinearLayoutManager(this)
//                it.setHasFixedSize(true)
//                it.adapter = ProductAdapter(listProduct)
//            }
//            })
        })
    }

}