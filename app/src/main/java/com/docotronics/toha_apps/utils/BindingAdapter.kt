package com.docotronics.toha_apps.utils

import android.text.TextUtils
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


object BindingAdapter {

    @BindingAdapter("error")
    @JvmStatic
    fun setError(editText: TextInputEditText, strOrResId: Any?) {
        if (strOrResId is Int) {
            editText.error = editText.context.getString((strOrResId as Int?)!!)
        } else {
            editText.error = strOrResId as String?
        }
    }

    @BindingAdapter("passwordValidator")
    fun passwordValidator(editText: TextInputEditText, password: String?) {
        val minimumLength = 5
        if (TextUtils.isEmpty(password)) {
            editText.error = null
            return
        }
        if (editText.text.toString().length < minimumLength) {
            editText.error = "Password must be minimum $minimumLength length"
        } else editText.error = null
    }

    @BindingAdapter("app:errorText")
    fun setErrorMessage(view: TextInputLayout, errorMessage: String?) {
        view.error = errorMessage
    }
}