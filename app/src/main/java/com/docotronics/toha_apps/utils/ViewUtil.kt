package com.docotronics.toha_apps.utils

import android.content.Context
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.google.gson.Gson

fun ObjToJSON(T : Any) : String{
    var gson = Gson()
    return gson.toJson(T)
}

fun Context.Toast(Message : String){
    Toast.makeText(this, Message, Toast.LENGTH_SHORT).show()
}

fun ProgressBar.show(){
    visibility = View.VISIBLE
}

fun ProgressBar.hide(){
    visibility = View.GONE
}


