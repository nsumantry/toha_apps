package com.docotronics.toha_apps.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.docotronics.toha_apps.MVVMApplication
import com.docotronics.toha_apps.R

class MainMenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
    }
}