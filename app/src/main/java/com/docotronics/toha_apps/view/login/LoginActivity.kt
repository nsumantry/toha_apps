package com.docotronics.toha_apps.view.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.docotronics.toha_apps.R
import com.docotronics.toha_apps.network.RetrofitApp.getLoginService
import com.docotronics.toha_apps.network.api
import com.docotronics.toha_apps.databinding.ActivityLoginBinding
import com.docotronics.toha_apps.network.ConnectionType
import com.docotronics.toha_apps.network.NetworkMonitorUtil
import com.docotronics.toha_apps.repositroy.LoginRepository
import com.docotronics.toha_apps.utils.Toast
import com.docotronics.toha_apps.utils.hide
import com.docotronics.toha_apps.utils.show
import com.docotronics.toha_apps.view.MainMenuActivity
import com.docotronics.toha_apps.view_model.LoginViewModel
import com.docotronics.toha_apps.view_model_factory.LoginViewModelFactory

class LoginActivity : AppCompatActivity(), LoginListener {
    private lateinit var viewModel : LoginViewModel
    private lateinit var binding : ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        InitClass()
    }

    private fun InitClass(){
        val loginService = getLoginService()
        val api = api()
        val repository = LoginRepository(loginService)
        val factory = LoginViewModelFactory(repository)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        viewModel.listener = this
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.progressBar.hide()
    }

    override fun onInvalid() {
        viewModel.loginBindingModel.observe(this, Observer { 
            if (it.userid.isNullOrBlank()){
                binding.txtUserName.error = "Belum diisi"
            }

            if (it.password.isNullOrBlank()){
                binding.txtPassword.error = "Belum diisi"
            }
        })
    }

    override fun onLogin() {
        binding.progressBar.show()
        binding.btnLogin.isEnabled = false
    }
    
    override fun onSuccess() {
        binding.progressBar.hide()
        binding.btnLogin.isEnabled = true

        var intent = Intent(this, MainMenuActivity::class.java)
        startActivity(intent)
    }

    override fun onFailed(message: String) {
        binding.progressBar.hide()
        binding.btnLogin.isEnabled = true
        Toast(message)
    }
}