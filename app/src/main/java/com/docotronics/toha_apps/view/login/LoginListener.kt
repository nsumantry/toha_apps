package com.docotronics.toha_apps.view.login

interface LoginListener {

    fun onInvalid()

    fun onLogin()

    fun onSuccess()

    fun onFailed(message : String)

}