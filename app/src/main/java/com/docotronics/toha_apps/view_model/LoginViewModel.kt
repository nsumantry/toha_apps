package com.docotronics.toha_apps.view_model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.docotronics.toha_apps.model.binding_model.LoginBindingModel
import com.docotronics.toha_apps.model.request_model.LoginRequest
import com.docotronics.toha_apps.repositroy.LoginRepository
import com.docotronics.toha_apps.network.ApiUtil.RC_00
import com.docotronics.toha_apps.utils.ApiException
import com.docotronics.toha_apps.utils.Courotines
import com.docotronics.toha_apps.utils.NoInternetException
import com.docotronics.toha_apps.view.login.LoginListener
import java.lang.Exception


class LoginViewModel(
        private val repository: LoginRepository
) : ViewModel(){

    var loginBindingModel : MutableLiveData<LoginBindingModel> = MutableLiveData<LoginBindingModel>()
    val BindingModel : LiveData<LoginBindingModel> = loginBindingModel
    var listener : LoginListener? = null

    init {
        loginBindingModel.value = LoginBindingModel()
    }

    private fun ValidationLogin() : Boolean{
        if (loginBindingModel.value?.userid.isNullOrBlank()){
            listener?.onInvalid()
            return false
        }

        if (loginBindingModel.value?.password.isNullOrBlank()){
            listener?.onInvalid()
            return false
        }
        return true
    }

    fun Login(){
        if (ValidationLogin()){
            var loginRequest = LoginRequest(
                    loginBindingModel.value?.userid,
                    loginBindingModel.value?.password
            )

            Courotines.main {
                try {

                    val baseResponse = repository.login(loginRequest)
                    if (baseResponse.rc == RC_00){
                        listener?.onSuccess()
                    }else{
                        listener?.onFailed(baseResponse.rm)
                    }

                }catch (E : ApiException){
                    listener?.onFailed(E.message!!)
                }catch (E : NoInternetException){
                    listener?.onFailed(E.message!!)
                }catch (E : Exception){
                    Log.e("ERROR", E.message!!)
                    listener?.onFailed("Terjadi kesalahan")
                }

            }
        }
    }



//    @Bindable
    //var BindingModel : MutableLiveData<LoginBindingModel> = MutableLiveData<LoginBindingModel>()

//    var bindingModel : MutableLiveData<LoginBindingModel> = MutableLiveData<LoginBindingModel>()
//        @Bindable get() = field
//        set(value) {
//            field = value
//            notifyPropertyChanged(BR.bindingModel)
//        }
//
//    var test = MutableLiveData<String>()
//    //    get() = BindingModel;
//
//    var testuser = MutableLiveData<String>()
//
//    fun Test(){
//        BindingModel.value?.userId = "sdsdsd"
//        BindingModel.value?.notifyPropertyChanged(BR.userId)
//        BindingModel.value?.userId = "111"
//
//    }
//
//    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
//
//    }
////
//    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
//        TODO("Not yet implemented")
//    }

//    @Bindable
//    val textUserId = MutableLiveData<String>().apply {
//        ""
//    }
//
//    val userId : LiveData<String>
//        get() = textUserId
//
//    @Bindable
//    val textPassword = MutableLiveData<String>().apply {
//        ""
//    }
//
//    val password : LiveData<String>
//        get() = textPassword
//
//    private val _validationResult  : MutableLiveData<String> = MutableLiveData()
//    val validationResult  :  LiveData<String>
//        get() = _validationResult
//
//    fun Validation(){
//        if (textUserId.value.toString().isBlank()){
//            _validationResult.value = "User id tidah boleh kosong"
//        }
//
//        if (textPassword.value.toString().isBlank()){
//            _validationResult.value = "Password tidah boleh kosong"
//        }
//    }
}