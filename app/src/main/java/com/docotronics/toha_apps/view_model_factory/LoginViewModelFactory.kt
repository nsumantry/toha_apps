package com.docotronics.toha_apps.view_model_factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.docotronics.toha_apps.repositroy.LoginRepository
import com.docotronics.toha_apps.view_model.LoginViewModel

class LoginViewModelFactory(
    private val repository : LoginRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(repository) as T
    }
}